
#include "Globals.h"
#include "GPY_Calculator.h"
#include "output.h"

void printContext(Stream& stream) {
  stream << F("Ignore 1st reading          : ") << (ignoreFirstReading ? "true" : "false") << ENDL;
  stream << F("Acquisition period          : ") << IsatisAcquisitionPeriod << F(" msec") << ENDL;
  stream << F("GPY_DefaultVocValue         : ") << GPY_DefaultVocValue << F(" V") << ENDL;
  stream << F("GPY_MinSignificantDeltaV    : ") << GPY_MinSignificantDeltaV << F(" V") << ENDL;
  stream << F("GPY_NbrOfSamples            : ") << GPY_NbrOfReadingsToAverage << ENDL;  
  stream << F("GPY_MaxDeltaInAverageSet    : ") << GPY_MaxDeltaInAverageSet << ENDL;  
  stream << F("GPY_RepeatBadQualityReadings: ") << GPY_RepeatBadQualityReadings << ENDL;
  stream << F("GPY_Sensitivity             : ") << GPY_SensitivityInMicroGramPerV << F(" (µg/m3)/V") << ENDL;
  stream << F("GPY_DefaultVocValue         : ") << GPY_DefaultVocValue << F(" V") << ENDL;
  stream << F("Fan On (during test)        : ") << useFan << ENDL;
}

void printData(int i, double outputV, double dustDensity, unsigned long readingDuration, bool quality, Stream& stream) {
  stream.print(i);
  stream << F(".   ");
  stream.print(outputV, numDecimalPositionsToUse);
  stream << F(" V, ") << (quality ?"Good    , " : "NOT GOOD, ");
  stream.print(GPY_Calculator::Voc, numDecimalPositionsToUse);
  stream << F(" V, ");
  stream.print(dustDensity, numDecimalPositionsToUse);
  stream << F(" µg/m3. ---  Min outputV=");
  stream.print(minOutputV, 2);
  stream << F(" V, maxOutputV=");
  stream.print(maxOutputV, 2);
  stream << F("V, delta=");
  stream.print(maxOutputV - minOutputV, 2);
  stream << F("V, reading duration =");
  stream.print(readingDuration);
  stream << ENDL;
}

void printResults(Stream &stream) {
  stream << F("OutputV : [");
  stream.print(minOutputV, 2);
  stream << F(";");
  stream.print(maxOutputV, 2);
  stream << F("] V, delta=");
  stream.print(maxOutputV - minOutputV, 2);
  stream << F(" V, centre=");
  stream.print((maxOutputV + minOutputV) / 2, 2);
  stream << F(" V, average=");
  stream.println(totalOutputV / numReadings, 2);

    stream << F("OutputV (ok only) : [");
  stream.print(minOkOutputV, 2);
  stream << F(";");
  stream.print(maxOkOutputV, 2);
  stream << F("] V, delta=");
  stream.print(maxOkOutputV - minOkOutputV, 2);
  stream << F(" V, centre=");
  stream.print((maxOkOutputV + minOkOutputV) / 2, 2);
  stream << F(" V, average=");
  stream.println(totalOkOutputV / numOK_Readings, 2);

  stream.print("Quality OK: ");
  stream << ((float) numOK_Readings) / ((float) numReadings) *100 << F("%") << ENDL;
  
  stream << F("Dust density : [");
  stream.print(minDensity, 2);
  stream << F(";");
  stream.print(maxDensity, 2);
  stream << F("] µg/m3, delta=");
  stream.println(maxDensity - minDensity, 2);

  stream << F("Dust density (ok only) : [");
  stream.print(minOkDensity, 2);
  stream << F(";");
  stream.print(maxOkDensity, 2);
  stream << F("] µg/m3, delta=");
  stream.print(maxOkDensity - minOkDensity, 2); 
  stream << F(" µg/m3, average=");
  stream.print(totalOkDensity / numOK_Readings, 2);
  stream << F(" µg/m3") << ENDL;
  
  stream << F("Reading duration : [");
  stream.print(minReadingDuration);
  stream << F(";");
  stream.print(maxReadingDuration);
  stream << F("] msec, deltaT=");
  stream.print(maxReadingDuration - minReadingDuration);

  stream << ENDL;
}

