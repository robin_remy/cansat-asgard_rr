#include <Wire.h>
#define DEBUG_CSPU
#include "DebugCSPU.h"

#include "ClockSynchronizer.h"
#include "IsaTwoConfig.h"

constexpr byte CtrlLine = ImagerCtrl_MasterDigitalPinNbr;
constexpr unsigned long DurationOfImagingPhase=15; // seconds
constexpr unsigned int DelayAfterCtrlLineDown=1;   // Msec after going down with ctrl line
constexpr unsigned int DelayAfterCtrlLineUp=2000; // msec after going up with ctrl line

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial << "============================================" << ENDL;
  Serial << "=    Master/Imager synchro demonstrator    =" << ENDL;
  Serial << "=              (Master side)               =" << ENDL;
  Serial << "============================================" << ENDL << ENDL;

 
  Serial.print("CtrlLine =");
  Serial.println(CtrlLine);
  Serial << "Imaging during " << DurationOfImagingPhase << " seconds per cycle" << ENDL;
  Serial << "Delay after lowering the Ctrl line: " << DelayAfterCtrlLineDown << " msec" << ENDL;
  Serial << "Delay after moving the Ctrl line up: " << DelayAfterCtrlLineUp << " msec" << ENDL;
  Serial << "Imager I2C address:  0x";
  Serial.print(I2C_SlaveControllerAddress, HEX);
  Serial << " (" << I2C_SlaveControllerAddress << ")" << ENDL;
  
  pinMode(CtrlLine, OUTPUT);
  digitalWrite(CtrlLine, HIGH);
  Wire.begin();
}

unsigned long counter = 0;

void loop() {
  Serial.println();
  Serial.print(counter++);
  Serial.print(": Starting cycle...");
  digitalWrite(CtrlLine, LOW);
  delay(DelayAfterCtrlLineDown);
  Serial.println("transmitting");
  if (ClockSynchronizer_Master::setMasterClock(I2C_SlaveControllerAddress,1)) {
    Serial.println("Transmitted. Check clock synchro...");
    for (int i = 0; i<DurationOfImagingPhase; i++) {
      delay(1000);
      Serial.print(" clock=");
      Serial.println(millis());
    }
    Serial.println("End of cycle");
    digitalWrite(CtrlLine, HIGH);
    delay(DelayAfterCtrlLineUp); // Delay to make sure imaging is over
  }
  else {
    Serial.println("Error in transmission");
    digitalWrite(CtrlLine, HIGH); // cancelling to avoid deadlock.
    delay(DelayAfterCtrlLineUp); // Delay to make sure the image is over. 
  }

}
