/*
   Sketch to compare the performance of the LM35 sensor and the
   thermistor.

   Wiring:  LM35 to GND-VSS-Vout to A1
            thermistor in series with 10k resistor:
               Vss to thermistor, A0 to mid-point, resistor to GND.
            BMP280.
            Pmod TMP2 (using ADT7420 sensor). 
*/


#include "IsatisConfig.h"
#include "Adafruit_BMP280.h"
#include "IsatisHW_Scanner.h"
#include "ExtEEPROM.h"
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "elapsedMillis.h"
#include "ADT7420.h"

const unsigned long MeasurementPeriod=3000L;
const int LM35_Input = A1;
const int ThermistorInput = A0;
const unsigned int NumLM35_Samples = 100;
const unsigned int NumThermistor_Samples = 10;
const unsigned int NumBMP_Samples = 1;
const unsigned int NumADT_Samples = 1;

const float LM35_Offset = 1;
const float ThermistorOffset = -0.8;
const float BMP_Offset = -1.7;
const float ADT_Offset = 0;

Adafruit_BMP280 bmp;
IsatisHW_Scanner hw;
bool useBMP=false, useADT=false;
elapsedMillis timer;
  
void setup() {
  DINIT(19200);
  hw.IsatisInit();
  useBMP=hw.getFlags().BMP_SensorAvailable;
  useADT=hw.getFlags().ADT_SensorAvailable;
  hw.printFullDiagnostic(Serial);
  pinMode(LM35_Input, INPUT);
  pinMode(ThermistorInput, INPUT);
  Serial  << F("Testing LM35 on analog pin A1=#") << LM35_Input << F(" (") << NumLM35_Samples <<F(" sample(s)/reading)") << ENDL;
  Serial  << F("        thermistor on analog pin A0=#") << ThermistorInput << F(" (") << NumThermistor_Samples <<F(" sample(s)/reading)") << ENDL;
  Serial  << F("        BMP280 on I2C adress 0x77 (");
  Serial.flush();
  if (useBMP) {
     bool result= bmp.begin();
     Serial << NumBMP_Samples <<F(" sample(s)/reading)") << ENDL;
     if (!result) {
      Serial << F("Error initializing BMP280 library") << ENDL; 
     }
  }
  else {
     Serial << F("MISSING!") << ENDL;
  }
  Serial  << F("        ADT7420 on I2C address 0x4b (");
  Serial.flush();
  if (useADT) {
     bool result= ADT7420::init(I2C_ADT_SensorAddress);
     Serial << NumADT_Samples <<F(" sample(s)/reading)") << ENDL;
     if (!result) {
      Serial << F("Error initializing ADT7420 sensor") << ENDL; 
     }
  }
  else {
     Serial << F("MISSING!") << ENDL;
  }
  Serial << ENDL << F("Time, LM35 °C, therm °C, BMP280 °C, ADT7420 °C, pressure (hPa), time") << ENDL;

}

float roundToDot5(float in) {
  return in;  // Bypass this function: no rounding required, after all. 
  
  if (in < 0.0) return in; // negative numbers not supported.
  int temp = (int) (in*10.0); 
   
  switch (temp % 10) {
    case 0:
    case 1:
    case 2:
      temp = temp - temp %10;
      break;
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
      temp = temp - temp %10 + 5;
      break;
    default:
      temp = temp - temp%10 + 10;
  }
  return temp/10.0;
}

float readLM35() {
  float sum = 0.0;
  for (unsigned int i = 0; i < NumLM35_Samples; i++) {
    sum += 100.0 * analogRead(LM35_Input) * 5.0 / 1023.0;
  }
  return roundToDot5(sum / NumLM35_Samples)+LM35_Offset;
}

float readBMP() {
  if (!useBMP) return -1;
  float sum = 0.0;
  for (unsigned int i = 0; i < NumBMP_Samples; i++) {
    sum += bmp.readTemperature();
  }
  return roundToDot5(sum / (float) NumBMP_Samples)+BMP_Offset;
}

float readADT() {
  if (!useADT) return -1;
  float sum = 0.0;
  for (unsigned int i = 0; i < NumADT_Samples; i++) {
    sum += ADT7420::readTemperature(I2C_ADT_SensorAddress);
  }
  return roundToDot5(sum / (float) NumADT_Samples)+ADT_Offset;
}

float readThermistorValue()
{
  // Ref: https://www.element14.com/community/thread/42328/l/need-a-hand-reading-temperature-from-a-ntc-thermistor-10k-calibration?displayFullThread=true
  float sum = 0.0;
  for (unsigned int i = 0; i < NumThermistor_Samples; i++) {
    int rawADC=analogRead(ThermistorInput);
    // See http://en.wikipedia.org/wiki/Thermistor for explanation of formula
    float Temp = log(((10240000 / rawADC) - 10000));
    Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
    sum += Temp ;
  }
  return roundToDot5((sum/NumThermistor_Samples) - 273.15)+ThermistorOffset; // Convert Kelvin to Celsius
}

void loop() {
  if (timer < MeasurementPeriod) return;

  timer=0L;
  float time=millis();
  float LM35_Temp = readLM35();
  float thermistorTemp = readThermistorValue();
  float bmpTemp=readBMP();
  float adtTemp=readADT();
  float pressure=bmp.readPressure() / 100.0F;
  
  Serial << time << F(", ");
  Serial.print(time/1000, 5);
  Serial.print(F(", "));
  Serial.print(LM35_Temp,1);
  Serial.print(F(", "));
  Serial.print(thermistorTemp,1);
  Serial.print(F(", "));
  Serial.print(bmpTemp,1);
  Serial.print(F(", "));
  Serial.print(adtTemp,1);
  Serial.print(F(", "));
  Serial.print(pressure);
  Serial.print(F(", "));
  Serial << millis() << ENDL;
}
