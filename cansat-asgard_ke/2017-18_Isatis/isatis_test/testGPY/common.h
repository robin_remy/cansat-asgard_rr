#include "globals.h"

void resetGlobals();

void performOneReading( unsigned int readingId,
                        const byte nbrOfSamplesToAverage=GPY_NbrOfReadingsToAverage, 
                        const double maxDelta=GPY_MaxDeltaInAverageSet,
                        const bool repeatBadQualityReadings=GPY_RepeatBadQualityReadings,
                        const bool printResults=true) ;

byte selectFrom(  const __FlashStringHelper* question,
                  const __FlashStringHelper* answer[],
                  const byte numAnswers);

bool selectYN(  const __FlashStringHelper* question);
