/* 
 *  CO2_Client.cpp
 */

#include "CO2_Client.h"

CO2_Client::CO2_Client(byte theAnalogInPinNumber) {
  theAnalogInPinNumber = analogInPinNumber;
}

bool CO2_Client::begin(float theReferenceVoltage, int theDAC_StepNumber) {
  referenceVoltage = theReferenceVoltage;
  DAC_StepNumber = theDAC_StepNumber;
  return true;
}

void CO2_Client::readData(IsaTwoRecord& record) {
  record.co2 = analogRead(analogInPinNumber) * (referenceVoltage / DAC_StepNumber);
}
