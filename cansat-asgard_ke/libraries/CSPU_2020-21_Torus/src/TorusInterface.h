/*
 * TorusInterface.h
 *
 * Torus-specific definitions shared by space and ground segments
 */

#pragma once

/** State of the secondary mission controller. Only values 0-15 are allowed
 *  (coded on 4 bits in binary format).
 */
enum class TorusControlCode {
	NoData=0,			 /**< Controller not active */
	StartingUp=1,
	GroundConditions=2,  /**< Altitude lower than minimum: ground conditions
							 assumed (before take-off or after landing) */
	NotDescending=3,		 /** Can is either ascending, not yet ejected or too
							 recently ejected for the descent phase to be in progress */

	NominalDescent=4,	 /**< Can is descending with velocity in nominal range:  this
							  is the situation where the various flight phases
							  are managed. */
	LowDescentVelocityAlert=10,	/**< Can is descending with too low velocity:
									 correction to be made to increase velocity */
	HighDescentVelocityAlert=11 /**< Can is descending with escessive velocity:
									 correction to be made to decrease velocity */
};


