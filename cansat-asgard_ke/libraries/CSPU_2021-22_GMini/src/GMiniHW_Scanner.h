/*
 * GMiniHWScanner.h
 */

#pragma once

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "CansatHW_Scanner.h"
#include "GMiniXBeeClient.h"

/** A GMini-specific subclass of the Cansat hardware scanner.
 *  Required since we use a specific subclass of XBeeClient.
 */
class GMiniHW_Scanner: public CansatHW_Scanner {
public:
	GMiniHW_Scanner();

#ifdef RF_ACTIVATE_API_MODE
	virtual GMiniXBeeClient* getRF_XBee() override {
		return (GMiniXBeeClient*) CansatHW_Scanner::getRF_XBee();
	};

	virtual CansatXBeeClient* getNewXBeeClient() override {
		return new	GMiniXBeeClient(GroundXBeeAddressSH,GroundXBeeAddressSL);
	};
#endif


};

