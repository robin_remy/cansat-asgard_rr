// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "GMiniRecord.h"

#define DBG_BINARY 0

void GMiniRecord::printCSV_SecondaryMissionData(Stream & str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << (uint8_t) sourceID << separator;
  str << subCanEjected;
  if (finalSeparator) str << separator;
}

void GMiniRecord::printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << "sourceID,subCanEjected";
  if (finalSeparator) str << separator;
}

void GMiniRecord::clearSecondaryMissionData() {
  sourceID = CansatCanID::Undefined;
  subCanEjected = false;

}
uint16_t GMiniRecord::getSecondaryMissionMaxCSV_Size() const {
  return 1 /*,*/ + 5 /*sourceID (uint8_t)*/ + 1 /*,*/ + 4/*subCansatEjected (bool)*/;
}

uint16_t GMiniRecord::getSecondaryMissionCSV_HeaderSize() const {
  return 1 /*,*/ + 22 /*sourceID,subCansatEjected (amount of characters including comma)*/;
}

void GMiniRecord::printSecondaryMissionData(Stream& str) const {
  str << "sourceID: " << (uint8_t) sourceID << ENDL;
  str << "subCanEjected: " << subCanEjected << ENDL;
}

uint8_t GMiniRecord::writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const {
  uint8_t written = 0;
  uint8_t* dst = destinationBuffer;
  uint8_t remaining = bufferSize;
  written += writeBinary(dst, remaining, (uint8_t) sourceID);
  written += writeBinary(dst, remaining, subCanEjected);

  DPRINTS(DBG_BINARY, " writeBinary: sourceID=");
  DPRINTLN(DBG_BINARY, (uint8_t) sourceID);
  DPRINTS(DBG_BINARY, " writeBinary: subCanEjected=");
  DPRINTLN(DBG_BINARY, (int) subCanEjected); 

  return written;
}

uint8_t GMiniRecord::readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize) {
  uint8_t read = 0,id =0xFF;
  const uint8_t* src = sourceBuffer;
  uint8_t remaining = bufferSize;
  read += readBinary(src, remaining, id);
  sourceID= (CansatCanID) id;
  read += readBinary(src, remaining, subCanEjected);

  DPRINTS(DBG_BINARY, " readBinary: sourceID=");
  DPRINTLN(DBG_BINARY, (uint8_t) sourceID);
  DPRINTS(DBG_BINARY, " readBinary: subCanEjected=");
  DPRINTLN(DBG_BINARY, subCanEjected);

  return read;
}

uint8_t GMiniRecord::getBinarySizeSecondaryMissionData() const {
  return sizeof(uint8_t /* sourceID */) + sizeof(subCanEjected);
}
