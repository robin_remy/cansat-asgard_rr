/*
 * SecondaryMissionController.h
 */

#pragma once
#include "CansatRecord.h"
#include "elapsedMillis.h"

/** @ingroup CSPU_CansatAsgard
 *  @brief The class in charge of controlling the secondary mission during
 *  	   the flight (triggering actions etc.). This class is nevertheless
 *  	   not in charge of acquiring secondary mission data (this is
 *  	   implemented by the AcquisitionProcess class).
 *  	   This is a base class, to be sub-classed by each project to implement
 *  	   project-specific features.
 * 		   It possibly fills the record with secondary mission-related
 * 		   flight control information, if any.
 *
 *  @par Usage
 *  The project sub-class should override manageSecondaryMission() to perform the
 *  actual job, and possibly override:
 *  	- the begin() method, would it need additional initialization ;
 *  	- the prepareSecondaryMissionForShutdown() and cancelSecondaryMissionForShutdown()
 *  	  methods, would the secondary mission need particular action before
 *  	  can shutdown.
 *
 *  The subclass can be tested separately, by feeding it data records:
 *	@code
 *  	XxxxxSecondaryMissionController ctrl;
 *  	// once:
 *  	ctrl.begin();
 *
 *  	// in the main loop, providing appropriate data records. Beware that the
 *  	// run() method will only actually do anything at most once every
 *  	// CansatSecondaryMissionPeriod msec (parameter defined in CansatConfig.h).
 *  	ctrl.run(record);
 *  	// check the controller takes the appropriate actions, based on the data.
 *	@endcode
 *
 *  Operationally, the subclass is called transparently from within the CansatAcquisitionProcess,
 *  which takes care of calling begin() and run() at the right moments.
 *
 */
class SecondaryMissionController {
public:
	/** Constructor */
	SecondaryMissionController() {};
	virtual ~SecondaryMissionController() {};

	/** Initialize the controller before use.
	 * Override this method for project-specific initialization.
	 * @return true if initialization is successful, false otherwise.
	 */
	virtual bool begin() {return true;};

	/** The main entry point for the controller to be called from the
	 *  main loop. It manages the CansatSecondaryMissionPeriod (defined in
	 *  CansatConfig.h) to make sure manageSecondaryMission is called with
	 *  the appropriate frequency.
	 *  This method should normally not be overridden by subclasses.
	 *  @param record The last record, already complete with all primary
	 *                mission data and possibly the secondary mission
	 *                sensors data collected by the AcquisitionProcesss
	 */
	virtual void run(CansatRecord& record);

	/** Perform any action required before the can is powered down
	 *  This method does nothing and should be overridden by the subclass
	 *  when anything significant is to be performed.
     * @return True if everything went as planned.
     */
	virtual bool prepareSecondaryMissionForShutdown() { return true;};

	/** Cancel any action performed to prepare the can for powering down,
	 *  so normal operation can be resumed.
	 *  This method does nothing and should be overridden by the subclass
	 *  when anything significant is to be performed.
	 *  @return True if everything went as planned.
	 */
	virtual bool cancelSecondaryMissionShutdown() {return true;};

protected:
	/** Actually manage the secondary mission. This method is called every
	 *  CansatSecondaryMissionPeriod msec (defined in CansatConfig.h).
	 *  This method should be overridden by each project.
	 *  @param record The last record, already complete with all primary
	 *                mission data and possibly the secondary mission
	 *                sensors data collected by the AcquisitionProcesss
	 */
	virtual void manageSecondaryMission(CansatRecord& /*record*/){};
private:
	elapsedMillis elapsed;
};
