/*
    Test program for class GPS_Client in library CansatAsgardCSPU
    (Enable/Disable function only).

    Pin #EnablePin is expected to be connected to the GPS Break-out EN.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG
#include "CSPU_Test.h"
#include "GPS_Client.h"
#include "Adafruit_GPS.h"

constexpr uint8_t EnablePin = 11;

#define SERIAL2 //define if data should be transfered via Serial2 
//   Feather M0 Express: TX is on D10 and RX on D11
//   ItsyBitsy M4: TX is on A2 and RX on A3.

#ifdef __AVR__
#include "SoftwareSerial.h"
SoftwareSerial mySerial(3 , 2);
#elif defined(ARDUINO_ARCH_SAMD)

#  ifdef SERIAL2
#  include "Serial2.h"
HardwareSerial &mySerial = Serial2;
#  else
HardwareSerial &mySerial = Serial1;
#  endif
#else
#error "Only AVR and SAMD architectures are supported"
#endif
const bool detailedOutput = true; // Set to true to print all values on Serial
CansatRecord rec;

unsigned long lastCycleChange = 0;
unsigned long lastCycleLength = 0;

int numErrors = 0;

bool checkFixAndFrequency(float targetFrequency, GPS_Client& g) {
  rec.clear();

  constexpr uint8_t NumSamples=10;
  int numReadings = 0;
  unsigned long firstTS=0, lastTS=0, fixTS=0, startTS=millis(), period;
  unsigned long targetPeriod = 1.0/targetFrequency * 1000.0;
  
  Serial << "Waiting for GPS fix...." << ENDL; 
  Serial << "  Checking... targetFrequency=" << targetFrequency << " Hz, targetPeriod=" << targetPeriod << " msec" << ENDL; 
  elapsedMillis elapsed;
  while (numReadings < NumSamples) {
    rec.clear();
    rec.timestamp = millis();
    g.readData(rec);
    if (!fixTS && g.hasFix()) {
      fixTS=millis();
    }
    
    if (rec.newGPS_Measures) {
      //rec.printCSV(Serial);  Serial << ENDL;
      if (numReadings == 0) {
        firstTS = lastTS = rec.timestamp;
      } else {
        if ((rec.timestamp - lastTS) > 2*targetPeriod) {
          // reset
          numReadings=0;
          Serial << "  Resetting... (delay > 2*targetPeriod) " << ENDL << "  ";
          elapsed=0;
        }
        lastTS = rec.timestamp;
      }
      numReadings++;
      Serial << ".";
    }
    delay(100);
    if (elapsed > targetPeriod*NumSamples*2) {
      Serial << ENDL;
      Serial << "  ...still waiting for GPS data..." << ENDL;
      elapsed=0;
    }
  }
  period = (rec.timestamp - firstTS) / numReadings;
  float freq = 1000.0 / period;
  bool result = (fabs(freq - targetFrequency) < 3.0); // Only make a gross estimate
  Serial << ENDL;
  Serial << "Fix acquired in " << (fixTS - startTS)  << " msec, lat = " << rec.GPS_LatitudeDegrees << ", long = " << rec.GPS_LongitudeDegrees << ENDL;
  Serial << "Estimated frequency is " << freq << "Hz. Target=" << targetFrequency << " Hz: "
         << (result ? "OK" : " *** Error ***") << ENDL;
  if (!result) numErrors++;

  return (result);
}

void setup() {
  DINIT(115200);
  Serial << "GPS_Client test for the enable/disable function" << ENDL;
  Serial << "    GPS Enable line assumed to be connected on pin #" << EnablePin << ENDL;

  {
    GPS_Client g(mySerial);
    Serial << "Calling gps.begin() with enable pin, initial status on. ..." << ENDL;
    //Pass parameter GPS_Client::Frequency::F...Hz where ... is 1, 5 or 10. If nothing is passed, 5 is default
    g.begin(GPS_Client::Frequency::F5Hz, EnablePin);
    checkFixAndFrequency(5,g);

    g.enable(false);
    if (!CSPU_Test::askYN("GPS should be off. Is it so ?")) numErrors++;
    Serial << "Enabling again and checking fix and period..." << ENDL;
    g.enable(true);
    checkFixAndFrequency(5,g);
    Serial << "Setting frequency to 1Hz" << ENDL;
    g.changeFrequency(GPS_Client::Frequency::F1Hz);
    checkFixAndFrequency(1,g);
    Serial << "Disabling GPS...";
    delay(2);
    Serial << "Enabling again..." << ENDL;
    g.enable(true);
    checkFixAndFrequency(1,g);

    Serial << "Setting frequency to 10Hz" << ENDL;
    g.changeFrequency(GPS_Client::Frequency::F10Hz);
    checkFixAndFrequency(10,g);
    Serial << "Disabling GPS...";
    delay(2);
    Serial << "Enabling again..." << ENDL;
    g.enable(true);
    checkFixAndFrequency(10,g);
  }

  Serial << ENDL << "------------------" << ENDL;
 {
    GPS_Client g(mySerial);
    Serial << "B. Calling gps.begin() with enable pin, initial status off. ..." << ENDL;
    //Pass parameter GPS_Client::Frequency::F...Hz where ... is 1, 5 or 10. If nothing is passed, 5 is default
    g.begin(GPS_Client::Frequency::F5Hz, EnablePin, false);
    if (!CSPU_Test::askYN("GPS should be off. Is it so ?")) numErrors++;
    Serial << "Enabling and checking fix & period..." << ENDL;
    g.enable(true);
    checkFixAndFrequency(5,g);

    g.enable(false);
    Serial << "Setting frequency to 1Hz while disabled, enabling and checking fix and period..." << ENDL;
    g.changeFrequency(GPS_Client::Frequency::F1Hz);
    g.enable(true);
    checkFixAndFrequency(1,g);

    g.enable(false);
    Serial << "Setting frequency to 10Hz while disable, enabling and checking fix and period..." << ENDL;
    g.changeFrequency(GPS_Client::Frequency::F10Hz);
    g.enable(true);
    checkFixAndFrequency(10,g);
  }

  Serial << "===============================" << ENDL;
  if (numErrors) {
    Serial << "End of job (*** " << numErrors << " error(s) ***)" ENDL;
  } else {
    Serial << "End of job (no error)." ENDL;

  }
}

void loop() {

}
