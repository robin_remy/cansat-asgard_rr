/*
   Thermistor.h
*/
#pragma once

#include "Arduino.h"
#include "VariableSerialResistor.h"
/**
 * @ingroup CSPU_Thermistors
 * @brief   An abstract class for all models of thermistors.
 * 			Wiring: from Vcc to a resistor (serial resistor), to the thermistor,
 *			to the ground.  Voltage is measured at the middle point using an
 *			analog pin from the µController.
 */
class Thermistor : public VariableSerialResistor {
  public:
  /**
   * @param theVcc the source level (in volts)
   * @param theAnalogPinNbr this is the number of the analog pin
   * @param theSerialResistor this is the value of the serial resistor, in ohms.
   */
	Thermistor(	const float theVcc,
    					const uint8_t theAnalogPinNbr,
						const float theSerialResistor)
    : VariableSerialResistor(theVcc, theAnalogPinNbr, theSerialResistor) {};
	virtual ~Thermistor() {};

    /** Obtain the temperature by reading the voltage (averaging several readings),
     *  derive the resistance of the thermistor and then the corresponding temperature.
     *  @return The resulting temperature in °C.
     */
    virtual float readTemperature()const = 0;
};
