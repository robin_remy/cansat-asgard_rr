#pragma once
#include "Arduino.h"

class ScrewLatch {
  public:

    bool begin ();
    void lock ();
    void unlock ();
    void stop ();

  private:

    const int enableBridge1 = 9;
    const int MotorForward1 = 6;
    const int MotorReverse1 = 5;
    const int DelayForwardLong = 1000;
    const int DelayForwardShort = 500;
    const int DelayBackwardsLong = 1000;
    const int DelayBackwardsShort = 500;

    int Power = 255; //Motor velocity between 0 and 255
};
