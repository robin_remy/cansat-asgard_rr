#ifndef ARDUINO_ARCH_SAMD
#error "This program is only intended for SAMD architectures."
#endif

#include <Adafruit_GPS.h>
#include "SAMD_InterruptTimer.h"
#include <assert.h>
#include "Serial2.h"

#define PMTK_SET_NMEA_OUTPUT_GGAONLY "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29" //This sentence doesn't appear in the official adafruit library but still gets accepted by the gps (allows the GPS to run at 10Hz)

const bool Use10Hz = true;
const bool UseSerial2 = false;
const uint32_t interruptPeriod = 1; // (ms)
const bool PrintGPS_Data = true;

HardwareSerial &serialForGps = (UseSerial2 ? Serial2 : Serial1);

SAMD_InterruptTimer gpsTimer;
Adafruit_GPS myGps(&serialForGps);

void TimerHandler() {
  char c = myGps.read();
}

void setup() {
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Serial link OK.");

  myGps.begin(9600);
  delay(1000); /**Gives time to the GPS to boot perfectly */

  myGps.sendCommand(PMTK_SET_NMEA_OUTPUT_GGAONLY);
  
  if(Use10Hz) {
    myGps.sendCommand(PMTK_SET_NMEA_UPDATE_10HZ);
  }

  gpsTimer.changePeriod(interruptPeriod);
  assert(gpsTimer.start(interruptPeriod, TimerHandler));

  Serial.println("Setup completed, begining tests.");
}

void loop() {
  bool newReceived = myGps.newNMEAreceived();
  if(newReceived) {
    bool result = myGps.parse(myGps.lastNMEA());
    if (!result) Serial.println("ERROR PARSING NMEA");

    bool fixOK = (myGps.fixquality == 1) || (myGps.fixquality == 2)  || (myGps.fixquality == 3);
    if(fixOK && PrintGPS_Data) {
      Serial.print("New data: "); Serial.print(myGps.longitudeDegrees); Serial.print(","); Serial.println(myGps.latitudeDegrees);
    }
  } 

  delay(10);
}
