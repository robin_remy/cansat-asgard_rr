#include"arduino.h"
class BlinkLED {
  public:
    bool begin(const uint8_t theLED_PinNbr, const uint16_t thePeriodInMsec);
    void run();
  private:
    uint8_t LED_PinNbr;
    unsigned long lastChangeTimestamp;
    uint16_t periodInMsec;
};
