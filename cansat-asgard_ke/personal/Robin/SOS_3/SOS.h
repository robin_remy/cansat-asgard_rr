#include "Arduino.h"
class SOS {
  public:
    bool begin (const uint8_t ledPinNbr);
    void blink();
  private:
    void SOS::flash(int duration);
    void SOS::writeS();
    void SOS::writeO();
    uint8_t pinNbr;
};
