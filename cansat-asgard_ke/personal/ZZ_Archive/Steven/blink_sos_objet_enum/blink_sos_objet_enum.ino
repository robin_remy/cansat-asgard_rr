#include "BlinkingLed.h"  //Include the headers that contain our objects
#include "SOSLed.h"       //
//Declaration of the objects
BlinkingLed led1(LED_BUILTIN, 1000);
BlinkingLed led2(10, 500);
SOSLed led3(8, 2000);
BlinkingLed led4(6, 4829);
SOSLed led5(4, 3254);
void setup() {
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly:
  led1.run();   //Call the functions
  led2.run();   //
  led3.run();
  led4.run();
  led5.run();

}
