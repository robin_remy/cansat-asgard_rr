/**
  Sketch simulating RT transmission from subcan 1.
  Test with Feather M0 or ItsyBitsy M4 board.

  The contents of the records are defined in GMiniRecordExample class .h
  (because it is shared by the various sketches.

  Wiring: µC to XBee module.
       Feather/ItsyBitsy
       3.3V to VCC
       RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
       TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
       GND  to GND

*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#  error "This program only works on SAMD boards (Feather MO_Express, ItsyBitsy M4 Express, etc.)"
#endif

#include "GMiniConfig.h"
#include "GMiniXBeeClient.h"
#include "GMiniRecordExample.h"
#include "CSPU_Test.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif
#undef DEBUG_TS   // Define to print out timestamps in incoming records


constexpr uint32_t  DefaultDestinationSH = GM_Ground_XBeeAddressSH;
constexpr uint32_t  DefaultDestinationSL = GM_Ground_XBeeAddressSL;
constexpr bool RegularlyDisplayChannel = false; // if true, the current channel is displayed
//                                                 every ChannelPrintPeriod ms (debug: disrupts
//                                                 RF link)
constexpr unsigned int StorageDelay = 20; // ms. The delay used to simulate storage to SD card.
constexpr unsigned int ChannelPrintPeriod = 20000; // ms
constexpr unsigned int StatsPrintPeriod = 30000; // ms
constexpr unsigned int SpontaneousStatusPeriod = 2500; // ms
constexpr unsigned int MaxLineLength = 75;
constexpr bool DetailedErrorMsgOnEmission = false; // if true, detailed error message when failing
//                                                    to send to ground.  (debug: disrupts
//                                                    RF link)
constexpr uint16_t RF_AckDelay = 0; // ms. 0 = don't wait for ack.

// ----------------- Globals ----------------------
HardwareSerial &RF = Serial1;
GMiniXBeeClient xbc(DefaultDestinationSH, DefaultDestinationSL); // Defined in GMiniConfig.h
GMiniRecordExample myRecord, incomingRecord;
char myString[xbc.MaxStringSize];
CansatFrameType stringType;
bool ownRecordsInterrupted = false, relayInterrupted = false, statusMsgInterrupted = false;
char myself[25];
unsigned long numRecordsOK[4] = {0, 0, 0, 0}; // Count OK records from subCans
unsigned long numRecordsKO[4] = {0, 0, 0, 0}; // Count KO records from subCans
unsigned long numRecordsToGroundOK = 0; // Count OK records to ground
unsigned long numRecordsToGroundKO = 0; // Count KO records to ground
unsigned long charCounter = 0;  // character counter to split lines.

elapsedMillis elapsed = 1000, elapsedSinceChannelPrint = 0,
              statsElapsed = 0, elapsedSinceLastStatus = 0, statsDuration=0;
unsigned long numErrors = 0;

// -------------------------------------------------------------------
void  printStats(bool forced = false) {
  if (forced || (statsElapsed > StatsPrintPeriod)) {
    statsElapsed = 0;
    auto duration = statsDuration;
    Serial << ENDL << "Main can stats (acq. period=" << CansatAcquisitionPeriod << " msec)" << ENDL;
    for (int i = 0; i < 4 ; i++) {
      Serial << "  From SubCan " << i + 1 << ": " << numRecordsKO[i] << "/" << numRecordsKO[i] + numRecordsOK[i]
             << " = " << (float) (100.0 * numRecordsKO[i]) / (numRecordsOK[i] + numRecordsKO[i]) << "% errors, avg. period="
             << (float) duration / numRecordsOK[i]  << " msec" << ENDL;
    }
    Serial << "  To Ground    : " << numRecordsToGroundKO << "/" << numRecordsToGroundKO + numRecordsToGroundOK
           << " = " << (float) (100.0 * numRecordsToGroundKO) / (numRecordsToGroundOK + numRecordsToGroundKO) << "% errors" << ENDL;
  }
}

/* Check all data members, check timestamp is previous timestamp+1
   return true if ok */
bool checkRecord(GMiniRecordExample& rec) {
  bool dataOK = rec.checkValues(true);
  static unsigned long currentTS[4] = {0, 0, 0, 0}; // the timestamp expected in next record.

  if (((int) rec.sourceID == 0) || ((int) rec.sourceID > 3)) {
    Serial << ENDL << myself << ": Error record.sourceID: got " << (int) rec.sourceID << " (ignored)." << ENDL;
    return false;
  }
  bool tsOK = false;
  if (currentTS[(int) rec.sourceID - 1] == 0) {
    currentTS[(int)rec.sourceID - 1] = rec.timestamp + 1;
    numRecordsOK[(int)rec.sourceID - 1]++;
    tsOK = true;
  } else {
#ifdef DEBUG_TS
    Serial << ENDL << "sourceID=" << (int) rec.sourceID << ", rec.ts=" << rec.timestamp;
    Serial << ", currentTS[sourceID-1]=" << currentTS[(int) rec.sourceID - 1] << ENDL;
#endif
    if (dataOK) numRecordsOK[(int)rec.sourceID-1]++;  // The record we received is ok. The lost ones are nok
    if (rec.timestamp == currentTS[(int) rec.sourceID - 1]) {
      tsOK = true;
    } else {
      int lost = rec.timestamp - currentTS[(int)rec.sourceID - 1];
      Serial << ENDL << myself << ": Error in timestamp: expected " << currentTS[(int)rec.sourceID - 1 ] << ", got " << rec.timestamp << ENDL;
      if (lost > 0) {
        Serial << myself << ": lost " << lost << " record(s)." << ENDL;
        numRecordsKO[(int)rec.sourceID - 1] += lost;
      } else {
        numRecordsKO[(int)rec.sourceID - 1]++;
      }
      charCounter = 0;
    }
    currentTS[(int)rec.sourceID - 1 ] = rec.timestamp + 1;
  }
  if (!tsOK) numErrors++;
  if (!dataOK) numErrors++;
  return (tsOK && dataOK);
}

void sentRecordToGround(GMiniRecord& rec) {
  char c = 'G';
  if (rec.sourceID != CansatCanID::MainCan) c = '0' + (int) rec.sourceID;
  if (xbc.send(rec, RF_AckDelay, GM_Ground_XBeeAddressSH, GM_Ground_XBeeAddressSL)) {
    Serial << c ;
    charCounter++;
    numRecordsToGroundOK++;
  } else {
    numRecordsToGroundKO++;
    if (DetailedErrorMsgOnEmission) {
      Serial << "*** " << myself  << ": Error sending record (" << ++numErrors << ")" << ENDL;
      Serial << "    Destination: "; PrintHexWithLeadingZeroes(Serial, GM_Ground_XBeeAddressSH);
      Serial << " - "; PrintHexWithLeadingZeroes(Serial, GM_Ground_XBeeAddressSL);
      Serial << ENDL;
    } else Serial << '*';
  }
  if (charCounter > MaxLineLength) {
    Serial << ENDL;
    charCounter = 0;
  }
}

// Perform whatever must be done with valid incoming record
void processRecord(GMiniRecord & rec) {
  // Simulate storage
  delay(StorageDelay);

  // Resend if required.
  switch (GMiniSelectedRF_Strategy) {
    case GMiniRF_Strategy::MainCanAsRepeater:
      // Resend to Ground
      if (!relayInterrupted) sentRecordToGround(rec);
      break;
    case GMiniRF_Strategy::BroadcastFromSubcans:
    case GMiniRF_Strategy::MulticastFromSubcans:
    case GMiniRF_Strategy::MultipleBindingsFromSubcans:
    case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
      // Nothing to do
      break;
    default:
      Serial << "Unexpected RF Strategy: (" << (uint) GMiniSelectedRF_Strategy << ") " << xbc.getLabel(GMiniSelectedRF_Strategy) << ENDL;
  } // switch

}

// Perform whatever must be done with incoming command.
void processCmdRequest(char* cmd) {
  Serial << 'R';
  xbc.openStringMessage(CansatFrameType::CmdResponse);
  xbc << "99, dummy response to dummy message";
  xbc.closeStringMessage();
}

void sendStatusMsg() {
  Serial << 'S';
  xbc.openStringMessage(CansatFrameType::StatusMsg);
  xbc << "Dummy spontaneous status message from the can!";
  xbc.closeStringMessage();
}

void checkRF() {
  bool gotRecord;
  char incomingString[xbc.MaxStringSize + 1];
  CansatFrameType stringType;
  uint8_t seqNbr;
  while (xbc.receive(incomingRecord, incomingString, stringType, seqNbr, gotRecord)) {
    if (gotRecord) {
      charCounter++;
      Serial << (int) incomingRecord.sourceID;
      if (checkRecord(incomingRecord)) {
        processRecord(incomingRecord);
      } else {
        Serial << '*';
      }
      if (charCounter > MaxLineLength) {
        Serial << ENDL;
        charCounter = 0;
      }
    } else {
      charCounter = 0;
      switch (stringType) {
        case CansatFrameType::CmdRequest:
          processCmdRequest(incomingString);
          break;
        case CansatFrameType::CmdResponse:
        case CansatFrameType::StatusMsg:
        case CansatFrameType::StringPart:
          Serial << myself << "Received unexpected '" << incomingString
                 << "' CmdRequest (ignored)" << ENDL;
          break;
        default:
          Serial << "*** " << myself << ": Error: unexpected string type: "
                 << (int) stringType << ENDL;
      }
    }
  } // While receive();
}

void checkSerialInput() {
  const char* Instructions = "'R'=resume all, 'r' = suspend/resume records, 'e'=suspend/resume retransmission (if applicable), 'm'=suspend/resume status msg, 'd'=detect nodes, 'p'=pause app., 'x'=XBee config...,'s'=print stats, 'z'=reset stats";
  char c;
  if ((c = CSPU_Test::keyPressed()) != 0) {
    switch (c) {
      case 'd':
        xbc.printDiscoveredNodes();
        Serial << ENDL << myself << ": " << Instructions << ENDL;
        break;
      case 'p':
        Serial << ENDL << myself << ": application paused" << ENDL;
        CSPU_Test::pressAnyKey();
        break;
      case 'r':
        if (!ownRecordsInterrupted) {
          Serial << ENDL << myself << ": record emission suspended. " << Instructions << ENDL;
        }
        else Serial << ENDL << myself << ": record emission resumed. " << Instructions << ENDL;
        ownRecordsInterrupted = !ownRecordsInterrupted;
        break;
      case 'e':
        if (!relayInterrupted) {
          Serial << ENDL << myself << ": Emission & relay suspended. " << Instructions << ENDL;
        } else Serial << ENDL << myself << ": Emission & relay resumed. " << Instructions << ENDL;
        relayInterrupted = !relayInterrupted;
        break;
      case 'm':
        if (!statusMsgInterrupted) {
          Serial << ENDL << myself << ": Status msg suspended. " << Instructions << ENDL;
        } else Serial << ENDL << myself << ": Status msg resumed. " << Instructions << ENDL;
        statusMsgInterrupted = !statusMsgInterrupted;
        break;
      case 'R':
        ownRecordsInterrupted = false;
        relayInterrupted = false;
        statusMsgInterrupted = false;
        Serial << myself << ": Resumed everything..." << ENDL;
        break;
      case 's':
        printStats(true);
        Serial << ENDL <<  myself << ": " << Instructions << ENDL;
        return;
      case 'x':
        xbc.printConfigurationSummary(Serial);
        Serial << myself << ": " << Instructions << ENDL;
        break;
      case 'z':
        statsElapsed = 0;
        statsDuration = 0;
        for (int i = 0; i < 4 ; i++) {
          numRecordsKO[i] = numRecordsOK[i] = 0;
        }
        printStats();
        Serial << ENDL <<  myself << ": stats reset. ";
        Serial << Instructions << ENDL;
        return;
      default:
        Serial << myself << ": ignored '" << c << "'. " << Instructions << ENDL;
    } // switch
  } // key pressed
} // checkSerial

void printCurrentChannel() {
  uint8_t channel;
  if (xbc.queryParameter("CH", channel) )
  {
    Serial << ENDL << "Current channel: " << channel << ENDL;
  } else {
    Serial << "*** Error querying channel" << ENDL;
  }
}

void sendRecord() {
  myRecord.timestamp++;
  switch (GMiniSelectedRF_Strategy) {
    case GMiniRF_Strategy::MainCanAsRepeater:
    case GMiniRF_Strategy::BroadcastFromSubcans:
    case GMiniRF_Strategy::MulticastFromSubcans:
    case GMiniRF_Strategy::MultipleBindingsFromSubcans:
    case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
      sentRecordToGround(myRecord);
      break;
    default:
      Serial << "*** " << myself << ": Error: unsupported RF_Strategy: '" << xbc.getLabel(GMiniSelectedRF_Strategy) << "'" << ENDL;
      Serial << "Aborted" << ENDL;
      Serial.flush();
      exit(-1);
  } // switch
}

void setup() {
  DINIT(115200);

  pinMode(LED_BUILTIN, OUTPUT);
  Serial << ENDL << ENDL;
  Serial << "***** MAIN CAN SKETCH *****" << ENDL;
  Serial << "Initializing Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value
  xbc.printConfigurationSummary(Serial);
  auto component = xbc.getXBeeSystemComponent();
  strcpy(myself, xbc.getLabel(component));

  // Check we have the right RF module connected.
  if (component != GMiniXBeeClient::GMiniComponent::MainCan) {
    Serial << "*** Error: wrong XBee module detected: " << myself
           << " instead of " << xbc.getLabel(GMiniXBeeClient::GMiniComponent::MainCan) << ENDL;
    Serial << "Aborted." << ENDL;
    Serial.flush();
    exit(-1);
  }
  // Check the module is properly configured
  if (!xbc.checkXBeeModuleConfiguration()) {
    Serial << "*** Error: fix module configuration with XBee_ConfigureGMiniModule.ino  before running the test!" << ENDL;
    Serial << "Aborted." << ENDL;
    Serial.flush();
    exit(-1);
  }

  // At this stage, the RF module is ok!
  myRecord.initValues();
  myRecord.sourceID = CansatCanID::MainCan;

  Serial << "Initialisation over. Test record content (size = " << sizeof(myRecord) << " bytes):" << ENDL;
  myRecord.print(Serial);
  Serial << "-------------------" << ENDL << ENDL;

  Serial << "Sending record to Ground (ID=" << xbc.getXBeeModuleID(DefaultDestinationSH, DefaultDestinationSL) << ", ";
  PrintHexWithLeadingZeroes(Serial, DefaultDestinationSH);
  Serial << " - ";
  PrintHexWithLeadingZeroes(Serial, DefaultDestinationSL);
  Serial << ")" << ENDL;
  Serial << "RF_Strategy: '" << xbc.getLabel(GMiniSelectedRF_Strategy) << "'" << ENDL;

  Serial << ENDL << "---------------------------------------" << ENDL;
  Serial << "Starting test.... (type any character+<return> for help)" << ENDL;
  Serial << " - Sending records every " << CansatAcquisitionPeriod << " ms" << ENDL;
  Serial << " - Sending a message to the ground every " << SpontaneousStatusPeriod / 1000.0 << " sec" << ENDL;
  Serial << " - Printing statistics every " << StatsPrintPeriod / 1000.0 << " sec" << ENDL;
  Serial << " 'G' = record from main can to ground, '1', '2' = record from subcan 1/2";
  Serial << " 'R' = response sent to ground cmd, 'S' = spontaneous status msg sent to ground" << ENDL;
  Serial << ENDL;
  statsElapsed = 0; statsDuration = 0;
}

void loop() {
  if (!ownRecordsInterrupted) {
    if (RegularlyDisplayChannel && (elapsedSinceChannelPrint > ChannelPrintPeriod)) {
      printCurrentChannel();
      elapsedSinceChannelPrint = 0;
    }
    if (elapsed > CansatAcquisitionPeriod) {
      elapsed = 0;
      sendRecord();
    }
  } // ! ownRecordsInterrupted

  // Always receive, send status, print stats etc.
  checkRF();
  if (!statusMsgInterrupted && (elapsedSinceLastStatus > SpontaneousStatusPeriod)) {
    elapsedSinceLastStatus = 0;
    sendStatusMsg();
  }
  checkSerialInput();
  CSPU_Test::heartBeat();
  printStats();
}
