/*  Test program compatiblity of the RGB_DotStar LED on the
    ItsyBitsy M4 and the GPS client using timers based on TIMER_TC_3 on SAMD21/SAMD51

    Turning off LED when timer is running is OK.
    Declaring a GPS_Client fails.
        Removing the static SAMD_InterruptTimer gpsTimer; from GPS_Client.cpp fixes it.
        Leaving GPS_Client and 
          just including "SAMD_InterruptTimer.h" at line XXX below) also fixes it???
          just including "SAMD_InterruptTimer.h" at line YYY below) result in compilation error 
           (unresolved symbol SAMD_InterruptTimer::SAMD_InterruptTimer(): this is because
           the library is linked too soon and the .o discarded because not need by the .ino.
           Arduino IDE limitation)

        Explored the possibility that the peculiar structurure of the SAMD_TimerInterrupt library
        (definining variables and functions in .h file) could be the source of the problem. 
        Changing into traditional .h/.cpp pairs of files does NOT solve the problem.
        
*/
#ifndef ARDUINO_ITSYBITSY_M4
#error "Test currently only supports ItsyBitsy M4"
#endif

#define CSPU_DEBUG
#include "CansatConfig.h"
#include "DebugCSPU.h"
#include "Adafruit_DotStar.h"
#include "CSPU_Test.h"
#include "Serial2.h"
//#include "SAMD_InterruptTimer.h"  // YYY
#include "GPS_Client.h"

//#include "SAMD_InterruptTimer.h"  // XXX 

void setup() {
  DINIT(115200);
  //Serial2.begin(115200);

  Serial << "Initializing GPS_Client" ;
  Serial << " on Serial2" << ENDL;
  GPS_Client gps(Serial2);
  //gps.begin();  
  
  // initialize here
  CSPU_Test::pressAnyKey();

  Serial << "Turning off RGB LED..." << ENDL;
  delay(500);
  Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
  Serial << "a" << ENDL;
  theDotStar.begin();
  Serial << "b" << ENDL;
  theDotStar.show(); // Shutdown
  Serial << "Dotstar now shut down" << ENDL;

  CSPU_Test::pressAnyKey();
  Serial << "End of job " << ENDL;
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly:

}
