/*
   System-wide configuration for the  ISATIS Project
*/

#pragma once

#include "Arduino.h"

// ********************************************************************
// ***************** Calibration settings to update frequently ********
// ********************************************************************
const float SeaLevelPressure_HPa = 1011.7;  // Sea level pressure value to calculate actual altitude.
const float AltitudeOffset = -317.91;           // Offset in m, added to every actual altitude calculation.
const float TemperatureOffset = -0.14;        // Offset in °C, added to every temperature reading.
const float PressureOffset = 35.98;             // Offest in hPa. 

// GPY_DefaultVocValue to define using testGPY (Calibration) and a ref. value AFTER all other settings are frozen
const double GPY_DefaultVocValue=0.43;                          //The initial value of the reference voltage associated with 0 density.

// GPY Settings below to define using testGPY (Stability)
const double GPY_MinSignificantDeltaV=0.15;                     //The minimum significant difference in output voltage, in volts.
const double GPY_MaxDeltaInAverageSet=0.4;                     //The maximum variation allowed in an average group to declare it
                                                               //acceptable. 
const byte GPY_NbrOfReadingsToAverage = 8;                     // The number of readings performed and averaged everytime a 
                                                               // GPY reading is requested.
const bool GPY_ForceLowDensityToZero=false;                    // If true, reading below GPY_MinSignificantDeltaV/2 are forced to 0
// GPY setting to evaluate with at least 2 ref. values and corresponding measurements.
const byte GPY_SensitivityInMicroGramPerV=200;                 // Sensivity of sensor, in (µg/m3)/V.                                                               

// ********************************************************************
// **************************** DEBUG SETTINGS ************************
// ********************************************************************
//#define IGNORE_EEPROMS             // When defined, detected EEPROM chips are ignored, in order to avoid wasting write cycles during tests
                                   // UNDEFINE THIS SYMBOL FOR OPERATIONS. 
#define PRINT_DIAGNOSTIC_AT_INIT   //Comment out to avoid startup diagnostic if memory usage must be reduced.
#define INIT_SERIAL
// Comment definition out to reduce program size if there is no use
// for the serial port at all (no debugging, no diagnostic at startup, etc.).

#define DEBUG_CSPU                     // Comment out to avoid including any debugging code. DEFINE FOR OPERATION
//#define USE_ASSERTIONS           // UNDEFINE THIS SYMBOL FOR OPERATIONS. 
//#define USE_TIMER                // Comment out to avoid timing output and overhead. UNDEFINE THIS SYMBOL FOR OPERATIONS
#include "DebugCSPU.h"             // include AFTER defining/undefining DEBUG_CSPU, USE_ASSERTION and USE_TIMER.

// Enable/disable the various parts of the debugging here:
#define DBG_SETUP 0
#define DBG_INIT  0
#define DBG_LOOP  0
#define DBG_ACQUIRE 0
#define DBG_STORAGE 0
#define DBG_DIAGNOSTIC 1
#define DBG_CAMPAIGN_STARTED 0
#define DBG_SLOW_DOWN_TRANSMISSION 0   // Add a 500 msec delay in the RF transmission 
#define DBG_GPY_VOC 0
#define DBG_GETOUTPUTV_AVERAGE 0

// IsatisHW_Scanner
#define DBG_CHECK_SPI_DEVICES 0

// IsatisAcquisitionProcess
//#define PRINT_ACQUIRED_DATA       // Define to have data printed in very readable format.
#define PRINT_ACQUIRED_DATA_CSV   // Define to have data printed in CSV (1 line/record) format

// ********************************************************************
// ************************ Mission parameters ************************
// ********************************************************************

const unsigned int IsatisAcquisitionPeriod = 400; // Acquisition period in msec. Min value, if RF_TransmissionDelayEveryTwoNumbers=0: 120 if PRINT_ACQUIRED_DATA is 
                                                  // not defined, 150 otherwise. If RF_TransmissionDelayEveryTwoNumbers>0, 
                                                  // check carefully the actual duration of the acquisition+storage+transmission.
const unsigned int IsatisCampainDuration = 300;   // Expected duration of the measurement campain in seconds.
const unsigned int IsatisSensorsWarmupDelay=1000; // Delay in msec to allow for the sensors to be ready (1 sec required for GPY). 
const float MinSpeedToStartCompaign = 1.2 ;       // Minimum speed (m/s) to detect during NumSamplesToStartCamapaign consecutive  
const byte NumSamplesToStartCamapaign = (1000/IsatisAcquisitionPeriod);        // samples to start fan and EEPPROM storage.
                                                  // WARNING: sensor accuracy is +- 1 m/s, so noise cause detection of 
                                                  // apparent speed of 1/acquisitionPeriod m/S. For 
                                                  // acquition period = 200 msec, this can be 5 m/s ! It is therefore
                                                  // essentiel to have NumSamplesToStartCamapaign cover at least 1 second.
const unsigned int SD_RequiredFreeMegs=100;       // The number of free Mbytes required on the SD card.
const uint16_t EEPROM_KeyValue=0x1216;            // The key identifying a valid EEPROM header. 

//#define USE_MINIMUM_DATARECORD          // If defined, the data record will not include derived data that can be computed offline.
const byte numDecimalPositionsToUse=5;  // The number of decimal positions to use in string representations of acquired data. 

// ********************************************************************
// **** GPY Stable settings (see calibration info on top of file) *****
// ********************************************************************

// The settings below define a 10 ms measuring cycle for a single reading (DO NOT MODIFY). 
const unsigned int GPY_DelayBeforeSamplingInMicroSec = 280;    //Delay (in µSec) after turning on GPY LED, before sampling
const unsigned int GPY_DelayAfterSamplingInMicroSec = 40;      //Delay (in µSec) after sampling, before turning GPY lED Off
const unsigned int GPY_DelayLED_OffInMicroSec = 9680;          //Delay (in µSec) after turning GPY LED Off.

const bool GPY_RepeatBadQualityReadings=false;                 // Repeat poor quality readings until quality is good. This causes
                                                               // reading time to become seconds long at times....
 //#define GPY_INCREASE_VOC                                    // Define to have the on the fly Voc adjustement increase it
                                                               // after GPY_NumSamplesToIncreaseVoc within an interval of GPY_MinSignificantDeltaV
                                                               // As far as we understand the isse, this makes no sense!!  
const byte GPY_NumSamplesToIncreaseVoc=5;                      // The number of sample of output voltage within GPY_MinSignificantDeltaV 
                                                               // which result in increase of Voc. No effect unless GPY_INCREASE_VOC
                                                               // is defined. 
                                                                      
// ********************************************************************
// ********************* Hardware configuration  **********************
// ********************************************************************

const  long serialBaudRate = 19200;               // baudrate on the serial interface of the µC board. Faster is best. 
                                                  // Max supported value: 74880
const  long serial1BaudRate = 19200;              // baudrate to communicate with the RF board (must be consistent with
                                                  // board settings.

// I2C Slaves
const byte I2C_lowestAddress = 0x4b;
const byte I2C_highestAddress = 0x7F;   // EEPROMS are at 0x50-0x57. 
const byte I2C_BMP_SensorAddress = 0x77;
const byte I2C_ADT_SensorAddress = 0x4b;
const byte unusedAnalogInPinNumber = 0; // An unused analog pin. Value read is used as random seed.

// Analog & digital pins allocation.
const byte GPY_dataOutPinNbr = A6 ;           //Connect the IO port of the GP2 sensor analog A0 output
const byte GPY_LED_VccPinNbr = 13 ;           //The pin in the GPY sensor that supplies power to the internal Led
const byte Fan_DigitalPin = 14;               // The pin used to control the FAN. 

const byte RF_TransmissionDelayEveryTwoNumbers=50; // The delay (in ms) after each 2 numbers sent on the RF interface, to avoid buffer overflow. 

#if defined ARDUINO_TMinus
// This is the operational configuration
const byte pin_HeartbeatLED = LED_BUILTIN;
const byte pin_InitializationLED = LED_BUILTIN1;
const byte pin_AcquisitionLED = LED_BUILTIN2;
const byte pin_StorageLED = LED_BUILTIN3;
const byte pin_TransmissionLED = LED_BUILTIN4;
const byte pin_CampaignLED = LED_BUILTIN5;
const byte pin_UsingEEPROM_LED = LED_BUILTIN6;
// SPI Slaves
const byte SD_CardChipSelect = 27; 

#elif (defined ARDUINO_AVR_UNO || defined ARDUINO_AVR_MICRO)
// This is an alternate configuration for development using a standard UNO board.
const byte pin_HeartbeatLED = 9; 
const byte pin_InitializationLED = 7;
const byte pin_AcquisitionLED = 2;
const byte pin_StorageLED = 3;
const byte pin_TransmissionLED = 5;
const byte pin_CampaignLED = 6;
const byte pin_UsingEEPROM_LED = 4;
// SPI Slaves
const byte SD_CardChipSelect = 8; 
#else
#error "Unexpected board. Please use either T-Minus or Uno, or complete the configuration file "
#endif



