/*
   SSC_StorageManager.h
   A class extending StorageManager specifically for the SpySeeCan project
*/

#pragma once
#include "StorageManager.h"
#include "SSC_Record.h"

class SSC_StorageManager : public StorageManager {
  public:
    SSC_StorageManager(unsigned int CampainDuration, unsigned int AcquisitionPeriod);
    virtual ~SSC_StorageManager() {};
    NON_VIRTUAL void storeSSC_Record(const SSC_Record& record, const bool useEEPROM);
};
