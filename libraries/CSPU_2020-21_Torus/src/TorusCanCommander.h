/*
 * TorusCanCommander.h
 */

#pragma once
#include "RT_CanCommander.h"
#include "TorusServoWinch.h"

#undef WAIT_AFTER_SET_WINCH_POSITION  // Define to have the commander wait for MaxDelayToReachTarget msec after each setPosition command.

/** @ingroup TorusCSPU
 *  @brief A subclass of RT_CanCommander adding support for the Torus-specific commands
 *  as specified in CansatInterface.h
 */
class TorusCanCommander : public RT_CanCommander {
public:
	/** @brief Constructor
        @param theTimeOut The duration (in milliseconds) for the Command mode to time out (and switch back to acquisition).
    */
	TorusCanCommander(unsigned long int theTimeOut);
	virtual ~TorusCanCommander() {};

#ifdef RF_ACTIVATE_API_MODE
    /** @brief Method used to initialize the object when using the RF API mode.
        @param xbeeClient The interface to the XBee module to used for output .
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.
        @param theServo   The asynchronous servoWinch object that can be targeted with commands.
     */
     void begin(CansatXBeeClient& xbeeClient, SdFat* theSd = NULL, AcquisitionProcess* theProcess = NULL, TorusServoWinch* theServo=NULL);
#else
    /** @brief Method used to initialize various pointers.
        @param RF_Stream The output stream used by the class.
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.
        @param theServo   The asynchronous servoWinch object that can be targeted with commands.
    */
    void begin(Stream& RF_Stream, SdFat* theSd = NULL, AcquisitionProcess* theProcess = NULL, TorusServoWinch* theServo=NULL);
#endif


protected:
    /** Process a project specific command request while in command mode.
     *  This method implements Torus-specific commands.
     * @param requestType the request type value
     * @param cmd	Pointer to the first character after the command type. Command parameter can be parsed from
     * 				this position which should point to a separator if any parameter is present, or to the final '\0'
     * 				if none is provided.
     * @return True if the command was processed, false otherwise.
     */
     virtual bool processProjectCommand(CansatCmdRequestType requestType, char* cmd) override;
     /** Perform any action required before the can is powered down
      * @return True if everything went as planned.
      */
     virtual bool prepareSecondaryMissionForShutdown() override;

     /** Cancel any action performed to prepare the can for powering down,
      *  so normal operation can be resumed.
      * @return True if everything went as planned.
      */
     virtual bool cancelSecondaryMissionShutdown() override ;

     void processReq_SetWinchPosition(char* &nextCharAddress);
     void processReq_GetWinchPosition(char* &nextCharAddress);
     void processReq_ResetWinchPosition(char* &nextCharAddress);

     /** Check whether a servoWinch is configured. If not send a CansatCmdResponseType::NoServoConfigured
      *  response
      *  @return true if a servo winch is ready to receive commands, false otherwise.
      */
     bool checkServo() const;

     /** Check whether a servoWinch is configured and running. If not send a CansatCmdResponseType::NoServoConfigured
      *  or a CansatCmdResponseType::WinchNotRunning
      *  response
      *  @return true if a servo winch is ready to receive commands and running, false otherwise.
      */
     bool checkServoRunning() const;

#ifdef WAIT_AFTER_SET_WINCH_POSITION
     static constexpr long int MaxDelayToReachTarget=10000; /**< The maximum delay for the winch to reach its target */
#endif
private:
     TorusServoWinch* servo; /**< The servo winch the CanCommander interacts with */
};

