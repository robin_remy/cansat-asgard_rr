/*
   GMiniAcquisitionProcess.h
*/

#pragma once
#include "CansatAcquisitionProcess.h"
#include "GMiniRecord.h"
#include "GMiniHW_Scanner.h"

#ifndef RF_ACTIVATE_API_MODE
#error "GMini project requires API mode to be activated on the RF interface"
#endif

/** @brief The acquisition process of GMini project
 *  this base class is subclassed for the main can and the sub can
*/
class GMiniAcquisitionProcess: public CansatAcquisitionProcess {
  public:
    GMiniAcquisitionProcess() {};
    virtual ~GMiniAcquisitionProcess() {};
  
  protected:
    /** @name Protected methods overridden from base class.
      	See detailed description in base classes.
        @{   */

    virtual void doIdle() override  {
      CansatAcquisitionProcess::doIdle();
      // do our own background tasks here, if any
    };

    virtual CansatRecord* 	getNewCansatRecord () override {
      return new GMiniRecord;
    };

    virtual CansatHW_Scanner* getNewHardwareScanner() override {
    	return new GMiniHW_Scanner;
    }

    virtual void initSpecificProject () override {
      // Perform specific project initialisation here
    };

    virtual void acquireSecondaryMissionData (CansatRecord &) override {
      // Read secondary mission sensors and populate record here
    } ;
    /** @} */

};
