/*
   LSM9DS0_IMU.h

   Possible improvement: Add support for SPI (just add constructors).
*/

#pragma once
#include "Adafruit_LSM9DS0.h"

/**  @ingroup CSPU_CansatAsgard
 *  @brief An interface to the LMS9DS0 sensor board providing complete calibration services. 
 *  
 *  It allows to configure the sensors and the calibration process and provides methods to obtain values abtained after
 *  applying calibration, in International System units: m/s^, µTesta and rad/s.
 *  
 *  _Accelerometers calibration_ \n
 *  DESCRIPTION TO BE COMPLETED, REF TO DESIGN DOCUMENT
 *  
 *  _Magnetometers calibration_ \n
 *  DESCRIPTION TO BE COMPLETED, REF TO DESIGN DOCUMENT
 *  
 *  _Gyroscopes calibration_ \n
 *  DESCRIPTION TO BE COMPLETED, REF TO DESIGN DOCUMENT
 *  
 *  
 */
class LSM9DS0_IMU {

  public:
    /** The soft-iron compensation matrix is a 3x3 float matrix */
    typedef float SoftIronMatrix_t[3][3];
    /** For readability, one of several aliases for a vector of floats (x,y,z directions). Used for offset vectors. */
    typedef float OffsetVector_t[3];
    /** For readability, one of several aliases for a vector of floats (x,y,z directions) Used for resolution vectors. */
    typedef float ResolutionVector_t[3];
    /** For readability, one of several aliases for a vector of floats (x,y,z directions) Used for vectors of values read from the sensor. */
    typedef float ReadingsVector_t[3];

    /** Constructor, for use on the I2C bus. Full scale values default to LSM9DS0_ACCELRANGE_2G,
        LSM9DS0_MAGGAIN_2GAUSS and LSM9DS0_GYROSCALE_245DPS, respectively.
    */
    LSM9DS0_IMU(int32_t sensorID = 0);

    /** Establishes the communication with the sensors, and performs the default configuration of the sensors.
        This method must be called before any other.
        @return True if the sensor is operational, false otherwise.
    */
    bool begin();

    /** Define the full-scale value for the accelerometers.\n
        __Warning__: the call overwrites the content of the resolutions vector to the theoretical
                     values for this range. Call configureAccelCalibration() AFTER setting the range.
        @param range: The full scale range (as defined in Adafruit_MSL9DS0.h).
    */
    void setupAccel(Adafruit_LSM9DS0::lsm9ds0AccelRange_t range);

    /** Define the full-scale value for the magnetometers.
        @param gain: The magnetometer gain, defined by the full-scale value (as defined in Adafruit_MSL9DS0.h).
    */
    void setupMag(Adafruit_LSM9DS0::lsm9ds0MagGain_t gain);

    /** Define the full-scale value for the gyroscopes.
        @param scale: The full scale range (as defined in Adafruit_MSL9DS0.h).
    */
    void setupGyro(Adafruit_LSM9DS0::lsm9ds0GyroScale_t scale);
 
    /** Configure accelerometers zero-g levels
       __Warning__: be sure to calibrate with values defined using the same scale and to call this
                    method AFTER the call to setupAccel(), which overwrites the resolutions vector.
       @param zeroG_Levels Vectors of zero-g levels to be used to offset the RAW values of the
              accelerometers (will be copied)
       @param resolutions Vector of accelerometer resolution in each direction (m/s^2 per raw step)
    */
    void configureAccelCalibration(const OffsetVector_t &zeroG_Levels, const ResolutionVector_t &resolutions);

    /** Configure magnetic calibration calculation.
         Warning: be sure to calibrate with values defined using the same gain
         @param offsets Vector of offsets to be applied to raw data (will be copied).
         @param softIronMatrix The 3x3 matrix to apply to raw data to correction for soft iron.
                        (will be copied)
    */
    void configureMagCalibration( const OffsetVector_t &offsets, const SoftIronMatrix_t &softIronMatrix);

    /** Configure gyro zero-rates
        Warning: be sure to calibrate with values defined using the same scale.
        @param zeroRateLevels Vectors of zero-rate levels to be used to offset the gyroscopes (will be copied)
    */
    void configureGyroCalibration(const OffsetVector_t &zeroRateLevels);

    /** Read all sensors and store raw data, and read timestamp. Access the values using the getXXXX() methods. */
    void readSensors();

    /** Get accelerometers readings after calibration and conversion to m/s^2
        @param values The array filled with the readings.
    */
    void getAccelValues(ReadingsVector_t &values);

    /** Get magnetometer readings after calibration and conversion to m/s^2
        @param values The array to be filled with the readings.
    */
    void getMagValues(ReadingsVector_t &values);

    /** Get gyroscopes readings after calibration and conversion to m/s^2
        @param values The array to be filled with the readings.
    */
    void getGyroValues(ReadingsVector_t &values);

    /** Get the timestamp of the last sensor reading 
        @return The timestamp corresponding to the last sensor reading.
        */
    uint32_t getReadTimestamp() {
      return _readTimestamp;
    } ;

    /** Utility function, for testing: output the calibration information, in a convenient format to
        read by the ground software.\n
        NB: This method is implemented in a separation translation unit (.cpp file) in order to avoid
        it to be linked with the operational code. 
    */
    void streamCalibrationData(Stream &str) const;

  private:
    /** A utility method to copy arrays of 3 floats.
     *  @param src The array from which values are copied.
     *  @param dest The array into which values are copied.
     */
    void copyVector(const float src[3], float dest[3]) {
      for (unsigned int i = 0; i < 3; i++) dest[i] = src[i];
    }

    /** Apply the calibration on the raw magnetic values: apply offset to center,
     *  apply soft-iron compensation matrix to restore sphericity of the measures surface
     *  and normalise the radius. 
     *  @param rawValues IN: the raw values as provided by the sensor, 
     *                   OUT:the raw values, after application of calibration (see above)
     */
    void getCalibrateRawMagData(ReadingsVector_t &rawValues);

    Adafruit_LSM9DS0 _lsm; /*!< The instance of the Adafruit object defining all hardware constants,
                     implementing the access to the various registers and providing access to
                     the raw data. Warning: as of v1.0.3 there are various bugs and issues
                     regarding conversion to I.S units. */
    OffsetVector_t _accelZeroG{};       /*!< The accelerometer offset in each direction, obtained during calibration (default to 0), in raw units */
    ResolutionVector_t _accelResolution{};  /*!< The accelerometer resolutions in each direction */
    OffsetVector_t  _magOffset{};    /*!< The magnetometer offsets obtained during calibration in each direction (default to 0), in raw units */
    SoftIronMatrix_t _magSoftIronMatrix{};  /*!< The soft iron compensation matrix obtained during calibration
                                                (3x3 matrix, defaults to unity).*/
    float  _magRefRadius{};  /*!< The radius used to normalise raw magnetometers data (initialized with first measure) */
    float  _magResolution{}; /*!< The resolution of the magnetometers, in µT */
    OffsetVector_t  _gyroZeroRate{};   /*!< The gyrometer offset in each direction obtained during calibration, in rad/s (defaults to 0) */
    float  _gyroResolution{}; /*!< The resolution of the magnetometers, in rad/s */
    uint32_t _readTimestamp{}; /*!< The timestamp for the last reading of the sensors */
  public:
    // This needs to be declared AFTER _lsm, because initialisation requires _lsm to be unitialised.
    Adafruit_LSM9DS0::lsm9ds0Vector_t &accelData; /*!<  The accelerometer raw data is available here */ 
    Adafruit_LSM9DS0::lsm9ds0Vector_t &magData;   /*!<  The magnetometer raw data is available here */ 
    Adafruit_LSM9DS0::lsm9ds0Vector_t &gyroData;  /*!<  The gyroscopes raw data is available here */ 

};

