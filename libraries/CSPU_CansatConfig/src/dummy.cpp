/*
 * dummy.cpp
 *
 * This file just makes sure the library compiles in ArduinoIDE
 * which has issues with empty libraries, and does not allow for
 * headers to be anywhere else than in libraries.
 * Thanks to the dot_a_linkage (cf. library.properties), dummy.o is
 * never included in the final executable.
 */
#pragma GCC diagnostic ignored "-Wunused-variable"
static int dummy;




