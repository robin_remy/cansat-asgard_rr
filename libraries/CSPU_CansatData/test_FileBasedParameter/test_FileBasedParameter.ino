/*
   test_FileBasedParameter.ino

    Test program for class FileBasedParameter

    An SD card must be available with chip-select on pin CS_Pin
    (easiest is to test with Feather M0 Adalogger).
*/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "CSPU_Debug.h"
#include "FileBasedParameter.h"

const byte CS_Pin = 4; // change to 10 if using Feather M0 express
SdFat sd;
uint16_t numErrors = 0;

void checkFileName(const char* paramName, const char* expectedFileName) {
  char fileName[13];
  FileBasedParameter::getParamFileName(paramName, fileName);
  if (strcmp(fileName, expectedFileName) != 0) {
    Serial << "*** Error: got '" << fileName << "' for parameter '" << paramName
           << "', instead of '" << expectedFileName << "'" << ENDL;
    numErrors++;
  }
}

void checkCWD(const char* expectedCWD_Name) {
  FatFile* currentDir = File::cwd();
  char name[20];
  if (!currentDir->getName(name, 20)) {
    numErrors++;
    Serial << "*** Error accessing current working directory name...." << ENDL;
  }
  if (strcmp(name, "/") != 0) {
    numErrors++;
    Serial << "*** Error: current working dir is '" << name << ", expected '" << expectedCWD_Name << "'" << ENDL;
  }
}

template<class T>
void checkValueRW(const char* label, const T value, const T defaultValue) {
  Serial << label << ": value=" << value << ", defaultValue=" << defaultValue << ENDL;
  if (!FileBasedParameter::write(sd, "dummyRW", value)) {
    numErrors++;
    Serial << "*** Error writing parameter...." << ENDL;
  }
  if (!sd.exists("/params")) {
    numErrors++;
    Serial << "*** Error : cannot find directory '/params'...." << ENDL;
  }
  checkCWD("/"); // check we didn't change CWD
  T dummyValue = 0;
  if (!FileBasedParameter::read(sd, "dummyRW", dummyValue, defaultValue)) {
    numErrors++;
    Serial << "*** Error reading parameter...." << ENDL;
  }
  if (fabs(dummyValue - value) > 0.00001) {
    numErrors++;
    Serial << "*** Error reading parameter: value=";
    Serial.print(dummyValue, 10);
    Serial << " instead of ";
    Serial.println(value, 10);
  }
}

void setup() {
  DINIT(115200)
  Serial << "Test program for class FileBasedParameter" << ENDL;
  if (!sd.begin(CS_Pin)) {
    Serial << "*** Error: could not initialise SdFat object (CS pin=" << CS_Pin << ")" << ENDL;
    Serial << "    Aborted." << ENDL;
    exit(-1);
  }

  checkCWD("/");
  Serial << "1. Checking parameter file names..." << ENDL;
  checkFileName("aaaa", "aaaa____.txt");
  checkFileName("aB", "aB______.txt");
  checkFileName("UnNomTropLong", "UnNomTro.txt");

  Serial << "2. Deleting all parameter files..." << ENDL;
  if (!FileBasedParameter::deleteAll(sd)) {
    numErrors++;
    Serial << "*** Error deleting all parameter files...." << ENDL;
  }

  checkValueRW("3a. read-write uint8_t", (uint8_t) 3, (uint8_t) 5);

  if (numErrors) exit(1);
  checkValueRW("3b. read-write uint16_t", (uint16_t) 12345, (uint16_t) 12346);
  checkValueRW("3c. read-write uint32_t", (uint32_t) 123456789, (uint32_t) 123456785);
  checkValueRW("3d. read-write int8_t", (int8_t) - 5, (int8_t) 2);
  checkValueRW("3e. read-write int16_t", (int16_t) - 532, (int16_t) 2);
  checkValueRW("3f. read-write int32_t", (int32_t) - 523456, (int32_t) 4);
  checkValueRW("4. read-write float", (float) 3.1765, (float) - 2.1);



  Serial << "5. Reading parameter inexistent integer parameter..." << ENDL;
  uint8_t value;
  if (FileBasedParameter::read(sd, "inexistent", value, (uint8_t) 10)) {
    numErrors++;
    Serial << "*** Error reading inexistent parameter...." << ENDL;
  }
  if (value != 10) {
    numErrors++;
    Serial << "*** Error reading parameter: value =" << value << " instead of 10" << ENDL;
  }

  Serial << "6. Reading parameter inexistent float parameter..." << ENDL;
  float fvalue;
  if (FileBasedParameter::read(sd, "finexistent", fvalue, 3.7f)) {
    numErrors++;
    Serial << "*** Error reading inexistent float parameter...." << ENDL;
  }
  if (fabs(fvalue - 3.7) > 0.001) {
    numErrors++;
    Serial << "*** Error reading parameter: value =" << fvalue << "instead of 3.7" << ENDL;
  }
  Serial << "7. Deleting dummy1 file..." << ENDL;
  if (!FileBasedParameter::deleteFile(sd, "dummy1")) {
    numErrors++;
    Serial << "*** Error deleting dummy1 file...." << ENDL;
  }
  Serial << "8. Deleting inexistent 'ABC' file..." << ENDL;
  if (!FileBasedParameter::deleteFile(sd, "ABC")) {
    numErrors++;
    Serial << "*** Error deleting inexistent file...." << ENDL;
  }

  Serial << "Clean-up: deleting all parameter files..." << ENDL;
  if (!FileBasedParameter::deleteAll(sd)) {
    numErrors++;
    Serial << "*** Error deleting all parameter files...." << ENDL;
  }
  checkCWD("/");
  Serial << "End of job. Number of errors: " << numErrors << ENDL;
}

void loop() {

}
