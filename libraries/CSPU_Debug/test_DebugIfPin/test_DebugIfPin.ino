/*
    test_DebugIfPin

    This sketch tests the DINIT_IF_PIN and DINIT_IF_ANALOG_PIN macroes


    Wiring:
       - pin DigitalDigitalActivationPin: either open or connected to ground. Internal pull-up is used.
       - pin AnalogDigitalActivationPin: External pull-up to Vcc required (large resistor, e.g. 1Mohm).
                                  Connected to ground or not to control activation.

    Usage:  run this sketch both with Arduino Uno and with another board which blocks if no terminal
            is actually connected to the USB Serial port, in each case, pull DigitalActivationPin up or down

       On Arduino UNO: whatever the state of DigitalActivationPin, the board should not block, and blink the
       on-board LED.
       If a serial monitor is connected: at init, a message should confirm the state of the DigitalActivationPin, and inform that
       the Serial port is not checked when the Activation pin is HIGH.
       If no serial monitor is connected: the LED should blink.

       On Feather M0 Express:
          -With Serial monitor connected and  DigitalActivationPin DOWN: Serial port should be properly initialized,
           and every msg should appear before the LED blinks.
          -With Serial monitor connected and  DigitalActivationPin UP: Serial port should be properly initialized,
           but program should start without waiting for the connexion to initialized: most setup messages are lost.
          -Without a Serial monitor connected (power the board with battery or USB-Charger) and  DigitalActivationPin DOWN:
           The LED never starts blinking: the board is blocked waiting for the Serial connexion in the setup.
          -Without a Serial monitor connected and  DigitalActivationPin UP: Serial port should be properly initialized,
           but not checked, the board does not wait, and the LED is blinking.



    With Uno: run the sketch


*/

#include "DebugCSPU.h"

constexpr byte DigitalActivationPin = 5;
constexpr byte AnalogActivationPin = A5;
constexpr byte LedPin = LED_BUILTIN;
constexpr long int baudRate = 115200;

constexpr bool TestWithAnalogPin=true;  // true to use the AnalogActivtationPin, false to use the DigitalActivationPin

//define DO_NOT_USE_DINIT_MACRO  // Define to not use the DINIT_xxx macroes. This is only to replace the macro with
                                 // different code while testing. 

void setup() {
#ifndef DO_NOT_USE_DINIT_MACRO
  // This is the actual test.
  if (TestWithAnalogPin) DINIT_IF_ANALOG_PIN(115200, AnalogActivationPin)
  else  DINIT_IF_PIN(115200, DigitalActivationPin);
#else
  // This is just to debug the macroes
  // DINIT(115200);
  Serial.begin(115200);
  if (analogRead(AnalogActivationPin) <= 25) {
    while (!Serial);
    Serial.println(F("Serial link OK"));
  } else {
    Serial.print(F("Serial link not checked (ADC="));
    Serial.print(analogRead(AnalogActivationPin));
    Serial.println(")");
  }
#endif
  if (TestWithAnalogPin) {
    Serial << "Testing with analog activation pin #" << AnalogActivationPin << ". Value is "
           << analogRead(AnalogActivationPin)  << ENDL;
  } else {
    Serial << "activation pin (#" << DigitalActivationPin << ") is "
           << ((digitalRead(DigitalActivationPin) == LOW) ? "LOW" : "HIGH")
           << ENDL;
  }
  pinMode(LedPin, OUTPUT);
  Serial << "Setup ok" << ENDL;
}

void loop() {
  digitalWrite(LedPin, !digitalRead(LedPin));
  Serial << "Running loop and blinking LED..." << ENDL;
  Serial << "Analog pin value: " << analogRead(AnalogActivationPin) << ENDL;
  delay(1000);
}
