/**
   This is a small sketch to check the use of XBee transmitters in TRANSPARENT mode.
   It prints whatever is received on the serial ports, byte by byte.
   It does not make use of any interface class and will not work if the XBee modules
   are configured in API mode.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

const int RF_rxPinOnUNO = 9;
const int RF_TxPinOnUno = 11;
char incomingByte;

#ifdef ARDUINO_ARCH_SAMD
HardwareSerial &RF = Serial1;
#else
#  include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#endif

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  RF.begin(115200);
  while(!Serial);
  Serial.println("Starting XBee Communication");
}

void loop() {

  if (RF.available() > 0) {
    incomingByte = RF.read();
    Serial.print("received : ");
    Serial.println (incomingByte);
  }

  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    Serial.print(incomingByte);
  }

}

