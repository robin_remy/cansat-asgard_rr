#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "SrewLatch.h"

ScrewLatch mySrew;

void setup() {
  DINIT(115200);
  myScrew.begin();
}

void loop() {
  myScrew.lock();
  delay(3000);
  myScrew.unlock();
  delay(3000);
  myScrew.stop();
  delay(3000);
};
