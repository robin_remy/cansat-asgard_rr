#pragma once
#include "Arduino.h"

typedef unsigned int EEPROM_Address;

class ExtEEPROM {
  public :

    /*
        Write a single byte into specified I2C EEPROM chip, at address.
        Returns true if successful, false in case of failure.
    */
    static bool writeByte(const byte I2C_Address, const EEPROM_Address address, const byte data);

    /*
        Read a single byte from specified I2C EEPROM chip, at address.
        Returns true if successful, false in case of failure.
    */
    static bool readByte(const byte I2C_Address, const EEPROM_Address address, byte &data);

    /*
         Write the first bufferSize bytes from buffer into specified I2C EEPROM chip, at address.
         Page write is used and write accross pages is supported (using the required number of page writes).
         Returns the number of bytes actually written.
    */
    static byte writeData(const byte I2C_Address, const EEPROM_Address address, const byte* buffer, const byte bufferSize);

    /*
        Read bufferSize bytes from specified I2C EEPROM chip, at address, and stores them in buffer
        Returns the number of bytes actually read.
    */
    static byte readData(const byte I2C_Address, const EEPROM_Address address, byte* buffer, const byte bufferSize);
};


